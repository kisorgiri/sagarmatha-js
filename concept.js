// element ==> angle bracket enclosed any keywords
// eg <html> </html> <p> <h2></h2>
// elements are used structure content in the window.
// browser will render(read and display) elements ==> not the element itself but the content inside element

// attribute
// attribute are propety that provide additional information of elements
// there will be specfic attribute for specific elements
// href ==>  a tag,linking css,
// src=> img script
// target => a
// width => img
// at => img

// Headings
// higher priority h1
// lower priority h6

// paragraphs
// p
// pre it maintain our own structure

// formatting(element)
// hr horizontal line
// br line break
// b, u, i ==> strong, ins, em
// small sm ==> small content then average
// subscript ==> lower then average text
// superscript ==> above then average text
// removed ==> del
// 
/*
commenting
single line comment and mutli line comment
*/
// list table
// list===> 3 types of list
// unordered list
// ordered list
// defination/description list


// block element and inline element
// block element
// ==> element that always starts from new line and take full width
// eg p pre hr br div
// inline element
// those element which will take only width that they can take
// eg sub sup strong em small span

// quotation
// internal source <q></q> // " "
// blockquote is used to specify external source 
// cite == which is used to give source
// cite can be used outside blockquote as well.

// forms
// forms is very important element in html
// forms is used to take input from end user
// form is an element



// css is cascading style sheet used to give style to content of web
// css version 3

// three ways to give style to your html
// 1. inline 
// 2 internal css 
// 3 external css