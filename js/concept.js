// development tools

// text editor or IDE (integrated development environment) webstorm 
// browser
// html file
// embedding js in js

//two way of embedding js in html
// 1.within a file
// 2 linking external file

// commenting in javascript inline
/*
    multi line
    comment
*/
// console.log('i am external file');

// general concepts

// operators
// arthemetic operators
// + addition
// - sub
// * multiplication
// / division
// % modulo (compute reminder)
// += if a = 2, b = 3, a += b; || (a = a+ b);
// -=
// *=
// /=
// ++ incremental operator
// --  decremental

// relation operator
// > greater then 
// < less then
// >= greater then equals
// <= less then equals

// equality operators
// ==, ===, !=   check equality value

// assignment operator
// = used to assign value

// logical operator/
// && logical and (given A and B both must be true) 
// || logical or (given A and B  any can be true)
// ! evaluates to false if given true and vice versa

// concatinate operator
// + , (comma)

// conditionals
// if, 
// if else
// if else if else
// switch-case

// loop (iteration)
// repetation of certain action untill condition is satisfied
// for //  count specific
// while, do-while // condition specific

// for in , forEach, reduce, map ,filter, every,some // js centric loop


// variables and constant
// variable and constant are memory allocation 
// declaration syntax of variables and constant
// var keyword is es5 standard 
// let keyword is es6 standard
// const  keywrod
// javascript (es) ECMA  es version es5 stable version es6 

// var name = 'sagarmata engineering college';
// var is varibale declaration syntax
// name name of variable
// = assigment operator
// anything that cames after assignment operator is value
// console.log('name == >'+name);


// general operator used
// '' single quote
//  " " double quote
// tild operator ~
// back tick operator `` 
// ^ caret operator
// parenthesis ()
// middlebraces {}
// square brackets []




