
// strcit mode
// "use strict"

// scope is accesibility and visibility of application data(function,constant,varibales)

// type of scope
// 1 global scope
// all the declaration at outermost layer of a file are global scope
// global scope are accessible within whole file
// 2 local (functional) scope
// all the declaration or data that remains inside function and accessible within function are functional scope
// 3 block scope
var text = 'welcome'; // global scope

function welcome(name) { // global scope
    "use strict"
    // random = 'sdkljfd';
    var innerText = 'you are warmly welcome'; // functional scope
    // name is also functional scope

    console.log('text >>', text); // accessible or not
    console.log('ineerText >>', innerText); // accessible or not
}
welcome('sagarmatha');
// console.log('inner text outside >>', innerText);

console.log('text outside function >>', text);

// console.log('chekcing welcomeText above function call >>', welcomeText);

function sayHello() {
    var innerText = 'i am inside sayhello';
    var welcomeText = 'i am sayhello text'; // if var/let const is not used it will treat as global variable
    console.log('text inside sayHello >>>', text);
    console.log('innerText >>', innerText);
}
sayHello();
// console.log('chekcing welcomeText below function call >>', welcomeText);



function buyPhone(model) {
    let availabelPhone = 's10';
    if (model == availabelPhone) {
        let newPhone = 'mi';
        console.log('avaibale phone >>', availabelPhone);
    } else {
        let availabelPhone = 'samsung';
    }
    console.log('avaible phone after if-else >>', availabelPhone);

}
// let will maintain block scope
// var will maintain functional scope
buyPhone('s10');
