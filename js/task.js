
// prepare a function to add two numbers

// function addition(a,b){
//     return a+ b;
// }
// hint closure

var result = addition(23)(33);
console.log('result >>', result); /// 56

// task 2
// prepare a function to calculate time
var time = '33m4s'; // '2m3s', 2m33s;
// expected result  
// {
//     minute: 33,
//         sec: 4
// }


var languages = ['rust', 'go', 'typescript', 'javascript', 'go', 'rust', 'go', 'typescript', 'javascript', 'go', 'python', 'python', 'rust', 'go', 'typescript', 'javascript', 'go', 'python'];

// 1st task
// find unique array of languages from given languages.

// 2nd task
// count the occurance of item from languages 
// expected result  is object
// {
//     rust: 3,
//         go: 5,
// }


// try to make functional

var students = [
    {
        name: 'ram',
        semester: 5,
        faculty: 'BCT',
        year: 3,

    },
    {
        name: 'ramesh',
        semester: 6,
        faculty: 'CSIT',
        year: 2,

    },
    {
        name: 'anu',
        semester: 5,
        faculty: 'BCT',
        year: 3,

    },
    {
        name: 'denish',
        semester: 6,
        faculty: 'ABC',
        year: 3,
    }

]

// prepare a function to group data by given propertyName

// function groupBy(source, groupByPropertyName) {
//     // logic
// }

// // expected result
// var semesterwise = groupBy(students, 'semester');
// // expected result in object
// {
//     5:[{},{},{}],
//     6:[{},{}],
//     3:[{}]
// }

// var facultyWise = groupBy(students, 'faculty');
// // expected result in object
// {
//     BCT:[{},{},{}],
//     CSIT:[{},{}],
//     ABC:[{}]
// }