// task part
function askForMoney(amt, cb) {
    // console.log('what comes in >>', cb);
    console.log('mom told me to wait till she receives salary');
    var askedAmt;
    setTimeout(function () {
        console.log('mom receives salary');
        // askedAmt = '40000';
        cb();
    }, 3000);
    // return askedAmt;
}

// execution part
console.log('i want to buy phone and i need money');
console.log('ask money with mom');
askForMoney(3000, function () {
    // blocking task
    console.log('i have money');
    console.log('i went to mobile shop to buy phone');

});
// non blocking task
console.log('go to college');
console.log('eat food');


// callback is a function which is passed as an argument to another function
// callback is mechanism to handle result of async call