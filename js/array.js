// inbuilt methods
// var languages = ['javascript', 'python', 'php', 'ruby', 'java', 'ruby'];
// console.log('languages >>', languages.length);
// console.log('find first index  of java>>', languages.indexOf('python'));
// console.log('last index  of java>>', languages.lastIndexOf('python'));
// indexOf will always return index of first occurance item
// lastIndexOf will always return index of last occurance item


// adding item in an array
// at first
// at last 
// somewhere in between
// console.log('original fruits >>', fruits);
// // add at first
// fruits.unshift('apple'); // unshift will return the length of array
// var a = fruits.push('banana');
// console.log('fruits >>', fruits);
// console.log('a >>', a);

// // remove from 1st
// var firstItem = fruits.shift();
// console.log('fruits >>', fruits);
// console.log('firstItem == >', firstItem);

// // remove from last
// var lastItem = fruits.pop();
// console.log('last item >>', lastItem);
// console.log("fruits .>", fruits);
// remove from somewhere between
//#### splice for removing
// var indexOfKiwi = fruits.indexOf('kiwi');
// fruits.splice(indexOfKiwi, 3);
// console.log('fruits after splice >>', fruits);

/// ### splice for adding
// fruits.splice(indexOfKiwi, 4,'papaya','watermelon','lemon');
// console.log('fruits >>', fruits);

// var fruits = ['grape', 'mango', 'kiwi', 'grape', 'pineapple', 'apple'];
// 
// loops
// forEach general purpose loop for array
// filter
// map
// reduce
// syntax
Array.reduce(function (acc, item, index, sourceArray) {

    // return 'something'
    // anything returned inside reduce is acc for next iteration
},'')
// we should provide initial acc value for reduce
// find

// fruits.forEach(function (item, index) {
//     console.log('item >>', item);
//     console.log('index >>', index)
// });


// function welcome(name) { //function accepting function is higher order function

// }
// welcome(function () { 

// }) // argument ma pass hune function lai hami callback vanincha

var cycles = [
    {
        name: 'abc',
        color: 'red',
        brand: 'cube'
    },
    {
        name: 'xyz',
        color: 'black',
        brand: 'giant'
    },
    {
        name: 'y250',
        color: 'blue',
        brand: 'twitter'
    },
    {
        name: 'abc',
        color: 'red',
        brand: 'cube'
    },
    {
        name: 'xyz',
        color: 'black',
        brand: 'giant'
    },
    {
        name: 'y250',
        color: 'blue',
        brand: 'twitter'
    },
    {
        name: 'abc',
        color: 'black',
        brand: 'cube'
    },
    {
        name: 'xyz',
        color: 'white',
        brand: 'giant'
    },
    {
        name: 'y250',
        color: 'green',
        brand: 'fury'
    },
];
console.log('cycles above <<', cycles);
cycles.forEach(function (item, index) {
    item.status = 'available';
});
console.log('cycle below >>>', cycles);

var cubeCycles = cycles.filter(function (item, index) {
    if (item.brand == 'cube') {
        return item;
    }
});
var blackCubeCycles = cubeCycles.filter(function (item, i) {
    if (item.color == 'black') {
        return true;
    }
});
console.log('cube cycle s>>>', blackCubeCycles);

cycles.map(function (item, i) {
    if (item.color == 'black' && item.brand === 'cube') {
        item.status = 'sold';
        return item;
    }
});
console.log('cycles >>', cycles.length);

cycles.forEach(function (item, i) {
    if (item.status == 'sold') {
        cycles.splice(i, 1);
    }
})
console.log('cycles lenght >>', cycles.length);







var numbers =[2,45,5,6,6,76,7,74,3,3];
// calculate sum of numbers using reduce

