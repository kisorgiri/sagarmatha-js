// 
// closure is a inner function which has access to
// 1 global scope
// parent function scope
// parent function arguments
// own scope
// own arguments



// pillers of OOP
// inheritance
// abstraction
// encapsulation // data hiding mechanism
// access specifiers(modifiers) public private protected
// polymorphism


var text = 'welome';

function welcome(name) {
    var innerText = 'hi';

    function setLocation(addr) { // closure
        var to = 'to';
        console.log(innerText + ' ' + name + ', ' + text + ' ' + to + ' ' + addr);
        return 'hi';
    }

    // setLocation(location);
    return setLocation;
}
var result = welcome('anu');
var callInner = result('nepal');
console.log('result >>', result);
console.log('call INNer >>', callInner);

