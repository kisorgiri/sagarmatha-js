// primitive datatype
// String
// Boolean
// Number
// Null 
// Undefined
// symbols

// non primitive data type
// Object
// Array

// inbuilt methods (method is function)
// typeof  check data type

// mutable properties and immutable properties
// Object collection of key value pairs
// // bracket notation
var apple = {
    color: 'red',
    origin: 'jumla',
    price: 222,
    quality: 'good',

}
var color = 'quality';
// accessing property of object
// dot notation
// bracket notation (recommeded to use with refrence)
console.log('data >>', apple.quality)
// adding property to object
apple.sold = true;

console.log('random', apple);

// bracket notation
console.log('apple >>', apple[color]); // loop
// JAVASCRIPT is loosely typed programming language
var stu = {
    name: 'ram',
    address: {
        permanent_addr: 'pokhara',
        temp_addr: ['bkt', 'ktm', 'lalitpur']
    }
}

// removing property of object
console.log('check above property >>', stu);

delete stu.name;

console.log('check deleted property >>', stu);

// array and object are non primitive data type that are used to hold multiple values

// array in javascript is hetregenous array
var fruits = ['apple', 'banana', 22, true, {}, []];// element item 
fruits[0]= 'mango';
// index 
console.log('fruits 0 index >>', fruits[0]);
