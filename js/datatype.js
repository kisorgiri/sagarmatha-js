// data type is classification of data 
// data classifcation in javascript
// primitive data type 
// String  any single quote or double quote enclosed value are string eg "33"
// Number any numeric value 2, 2.22
// Boolean true or false
// Undefined // declared but value not defined
// Null // non declared non allocated memory spaces
// Symbol internally used datatype to maintain uniqueness of property (key ) of object

var name = 'sagarmatha'; // string
var roll = 333; // number
var presentStatus = true; // boolean //
var location; // undefined
// var hobbies = 'coding,debugging,cycling,designing,testing';


// non primitive data type (for mutliple values)
// Array
// syntax of array
var hobbies = ['singing', 'coding', 'testing'];
// array sang sangai aaune kura k ho??
// index 
// array index always starts from 0
console.log('hobbies >>', hobbies[1]);
// Object

// IF YOU UNDERSTAND OBJECT YOU WILL UNDERSTAND JAVASCRIPT
// object is collection of key value pairs, name value pairs property value pairs
// eg
// var address = {
//     temp_addr: ['bkt', 'ktm', 'pkr'],
//     permanent_addr: 'illam'
// }
var anu = {
    presentStatus: true,
    roll_no: 9,
    email: 'anu@gmail.com',
    hobbies: ['singing', 'dancing'],
    height: 5.1,
    height: 6
}

// JAVASCRIPT is loosely typed programming language.(weekly dynamically typed)
var a;
a = 'sagarmataha';
a = 333;
a = true;
a = ['a'];
// a = {};
console.log('a >>', a);
