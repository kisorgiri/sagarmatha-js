// function is reusable block of code which performs specific task
// syntax
// function (){

// }
// two ways of defining a function
// 1. expression syntax
// JAVASCRIPT is loosely typed programming;
// welcome() // 
// 1 var welcome;
// welcome asiggnement occurs in line 13
// console.log('weclome >>', welcome);

var welcome = function () {
    // execution block
    console.log('i am expression syntax function');
}
//variable declaration


// this is expression syntax function

sayHello();

//2. declaration syntax
function sayHello() {
    console.log('i am declration syntax function');
}


// Hoisting
// Hoisting is mechanism which moves all the declaration at top before execution

// types of function
// 1. named function function with name take above example
// 2 anynamous function (unnamed function)
//  function(){}
// 3 function with argument
// 4 function with return type
// 5 IIFE (immidiately invoked functional expression)

// 3 function with argument

// function sendMail(name, address) { //place holder
//     // console.log('what comes in >', a)
//     // console.log('what comes in >', b);

//     var msgBody = 'Hi ' + name + ' , welcome to ' + address;
//     console.log('msgbody >', msgBody);
// }
// sendMail('rajesh', 'tinkune')
// sendMail('ram', 'ktm')
// sendMail('binod ', 'pokhara')
// sendMail(null, 'butwal')
// function sendMailAgain(arg) { //place holder
//     console.log('what comes in >', arg)
//     // console.log('what comes in >', b);

//     var msgBody = 'Hi ' + arg.name + ' , welcome to ' + arg.address;
//     console.log('msgbody >', msgBody);
// }
// var data = {
//     sender: 'ramesh',
//     receiver: 'test@gmail.com',
//     form: 'abcd',
//     msg: 'hi'
// }

// sendMailAgain(data);


// function with return type
function addition(num1, num2) {
    return num1 + num2;
}

var result = addition(44, 44);
console.log('what comes out >>', result);


function goToShop() {
    var fruits = ['apple', 'banana'];
    var vegitables = ['potato', 'pumpkin'];
    // es6 object shorthand notation
    console.log('i am above return');
    return {
        a: fruits,
        b: vegitables
    };
    return vegitables;
    // in any programming languages return pachadi code execute hudaina;

}
// var item = goToShop();
// console.log('result of go to shop >>', item);
// camelCase
// PascalCase
// skewer-case
// snake_case


(function(name){
    console.log('i am anynamous function ',name);
    console.log('i take help from IIFE to exectue')
})('sagarmatha');