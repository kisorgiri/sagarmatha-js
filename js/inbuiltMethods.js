// typeof ==> check data type of value

// String
var text = 'Welcome To Sagarmatha  ';
console.log('lenght >>', text.length);
console.log('original text >>', text);
console.log('uppercase >>', text.toUpperCase());
console.log('lowecase >>', text.toLowerCase());
console.log('remove white space >>', text.trim().length)

console.log('substr', text.substr(1, 5));
console.log('substring', text.substring(1, 5));

// type conversion 
// explict and implict 
// var num = '3333';
// console.log('type of num >>', typeof (num));
// // string to number
// var a = Number(num);
// console.log('a .>', typeof (a));
// string to array
var hobbies = 'coding,singing,dancing';
// hobbies.
// split
// console.log('split in array <>',hobbies.split('nepal'))


// number
// var num = 344;
// console.log('num to fixed decimal >>>', num.toFixed(2));
// console.log('int value only >>>', parseInt(num));
// console.log('parse float >>', parseFloat(num));


// function multiply(a, b) {
//     return a * b;
// }
// var res = multiply('sdsf2', 2);
// console.log('res >>', res);

// console.log('is nan check >>', isNaN('3jkj33'));
// console.log('res >>>', isFinite(null));

// Boolean
// ! logical not
// truthy and falsy properties
// var a = 'dslf'; 
// // a can be any truthy value ==> any defined value
// if(a){

// }
// // falsy value == > null,undefined, NaN, false ,'', 

var obj = {
    name: 'sagarmatah',
    address: 'sanepa',
    phone: 33333,
    email: 'sagarmatha@gmail.com',
}
console.log('check property >>', obj.hasOwnProperty('emailsdfd'));
// keys only in array
var keysOnly = Object.keys(obj)
console.log('keys only >>', keysOnly);
var valueOnly = Object.values(obj)
console.log('values only >>', valueOnly);

// type conversion
// serialization and deserialization
console.log('orignal object >>', obj);
var str = JSON.stringify(obj);
console.log('serilized object >>', str);
console.log('deserilized >>', JSON.parse(str));

// loop iteraton
// repatation of action untill certain condition is fullfiled
// enumerable properties
// for in  loop

for (var xyz in obj) {
    // console.log('loop count >>');
    console.log('key >>', xyz);
    console.log('value >>',obj[xyz])

    
}

