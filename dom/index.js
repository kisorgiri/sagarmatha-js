// console.log('i am external js file');
// var p = document.getElementById('test');
// // // var groupByclass = document.getElementsByClassName
// // // console.log('p', p);
// // console.log('p.inner html >>', p.innerHTML);
// // setTimeout(function () {

// //     p.innerHTML = 'DOM is very important';
// // }, 3000);
// var mainDiv = document.getElementById('main');
// var el = document.createElement('div');
// var ul = document.createElement('ul');
// var li = document.createElement('li');
// li.setAttribute('id', '322');
// li.innerHTML = "learn DOM";
// li.style.color ='red';

// li.setAttribute('class', 'abc');

// ul.appendChild(li);
// el.appendChild(ul);
// mainDiv.appendChild(el);

var mainDiv = document.createElement('div');
mainDiv.style.width = '400px';
mainDiv.style.height = '400px';
mainDiv.style.backgroundColor = "blue";
mainDiv.style.position = 'relative';
mainDiv.style.margin = '0 auto';


var animateDiv = document.createElement('div');
animateDiv.style.width = '50px';
animateDiv.style.height = '50px';
animateDiv.style.backgroundColor = 'yellow';
animateDiv.style.position = 'absolute';
animateDiv.style.bottom = '0px';
animateDiv.style.left = '0px';
animateDiv.style.borderRadius = '20px';
var img = document.createElement('img');
img.setAttribute('src', './wall.jpeg');
img.setAttribute('width', '50px');
animateDiv.appendChild(img);

mainDiv.appendChild(animateDiv);
var body = document.getElementsByTagName('body');
body[0].appendChild(mainDiv);

var btn = document.getElementById('btn');
btn.style.backgroundColor = 'green';
btn.style.color = 'white';
btn.addEventListener('click', function () {
    var position = 0;
    var interval = setInterval(move, 5);
    function move() {
        position++;
        if (position == 350) {
            clearInterval(interval);
        } else {
            animateDiv.style.bottom = position + 'px';
            animateDiv.style.left = position + 'px';
        }
    }

})
